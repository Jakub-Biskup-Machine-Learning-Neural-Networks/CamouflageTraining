﻿using System;
using UnityEngine;

public class PersonController : MonoBehaviour
{
    [SerializeField] private SpriteRenderer sRenderer;
    [SerializeField] private CapsuleCollider sCollider;
    [SerializeField] private float fitFactor;

    private DNA dna;

    public static Action OnPopulationTrainingStarted;

    private bool isDead = false;
    private DateTime birthTime;
    private TimeSpan lifeTimeSpan;

    public DNA DNA
    {
        get { return dna; }
        set {
            dna = value;
            sRenderer.color = dna.Color;
            transform.localScale = dna.Size;
            CalculateFitFactor();
            }
    }
    public float FitFactor { get { return fitFactor; } set { fitFactor = value; } }
    public TimeSpan LifeTimeSpan { get { return lifeTimeSpan; } }
    public bool IsDead { get { return isDead; } }

    private void Awake()
    {
        OnPopulationTrainingStarted = SetBirthTime;
    }

    private void SetBirthTime()
    {
        birthTime = DateTime.UtcNow;
    }

    public void OnPersonDeath()
    {
        isDead = true;
        lifeTimeSpan = DateTime.UtcNow - birthTime;
        sRenderer.enabled = false;
        sCollider.enabled = false;
    }

    public void CalculateFitFactor()
    {
        float colorDistFactor = Mathf.Sqrt(Mathf.Pow(sRenderer.color.r, 2) + Mathf.Pow(sRenderer.color.g, 2) + Mathf.Pow(sRenderer.color.b, 2));
        float normalizedColorDistFactor = Mathf.InverseLerp(Mathf.Sqrt(3), 0, colorDistFactor);
        float normalizedSizeFactor = Mathf.InverseLerp(1.33f, 0.33f, transform.localScale.x);


        fitFactor = Mathf.InverseLerp(0, 2, normalizedColorDistFactor + normalizedSizeFactor);
    }
}
