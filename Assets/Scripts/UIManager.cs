﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private Text genNumberText;
    [SerializeField] private Text timeText;

    private void Update()
    {
        RefreshVisual();
    }

    private void RefreshVisual()
    {
        genNumberText.text = string.Format("Generation: {0}", PopulationManager.generationNumber);
        timeText.text = string.Format("Test time: {0}", (int)PopulationManager.testElapsedTime);
    }
}
