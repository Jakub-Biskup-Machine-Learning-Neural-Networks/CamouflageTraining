﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DNA
{
    private Color color = Color.white;
    private Vector3 size = Vector3.one;

    public Color Color { get { return color; } set { color = value; } }
    public Vector3 Size { get { return size; } set { size = value; } }

    public DNA(Color color, Vector3 size)
    {
        this.color = color;
        this.size = size;
    }
}
