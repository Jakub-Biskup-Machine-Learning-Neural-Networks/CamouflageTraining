﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PopulationManager : MonoBehaviour
{
    [SerializeField] private GameObject personPrefab;
    [SerializeField] private Button startTrainingBtn;
    [SerializeField] int populationSize = 10;
    [SerializeField] float populationTestTime = 2f;
    [SerializeField] float variationDeviation = 0.025f;

    public static int generationNumber = 1;
    public static float testElapsedTime = 0f;
    private bool isStarted = false;

    private DateTime timeAfterLastElimination;

    private List<PersonController> currentPopulation = new List<PersonController>();

    public void OnStartTrainingButton()
    {
        GenerateFirstPopulation();

        timeAfterLastElimination = DateTime.UtcNow;
        PersonController.OnPopulationTrainingStarted();
        isStarted = true;
        startTrainingBtn.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (isStarted)
        {
            UpdateTestValues();
        }
    }

    private void GenerateFirstPopulation()
    {
        for (int i = 0; i < populationSize; i++)
        {
            Vector3 pos = GetRandomPersonPosition();
            PersonController person = Instantiate(personPrefab, pos, Quaternion.identity).GetComponent<PersonController>();
            person.DNA = GenerateRandomDNA();
            currentPopulation.Add(person);
        }

        currentPopulation = currentPopulation.OrderBy(x => x.FitFactor).ToList();
    }

    private DNA GenerateRandomDNA()
    {
        float r = UnityEngine.Random.Range(0f, 1f);
        float g = UnityEngine.Random.Range(0f, 1f);
        float b = UnityEngine.Random.Range(0f, 1f);
        float scale = UnityEngine.Random.Range(0.33f, 1.33f);

        return new DNA(new Color(r, g, b), new Vector3(scale, scale, scale));
    }

    private void UpdateTestValues()
    {
        TimeSpan diff = DateTime.UtcNow - timeAfterLastElimination;
        testElapsedTime += Time.deltaTime;

        if (testElapsedTime > populationTestTime)
        {
            OnPopulationTestEnded();
        }
        else if (diff.TotalSeconds >= (populationTestTime / populationSize))
        {
            timeAfterLastElimination = DateTime.UtcNow;
            EliminateOnePerson();
        }
    }

    private void EliminateOnePerson()
    {
        PersonController person = currentPopulation.FirstOrDefault(x => !x.IsDead);

        if (person != null)
        {
            person.OnPersonDeath();
        }
    }

    private void OnPopulationTestEnded()
    {
        testElapsedTime = 0;

        List<PersonController> sortedList = currentPopulation.OrderBy(x => x.FitFactor).ToList();
        currentPopulation.Clear();

        for (int i = 0; i < populationSize; i++)
        {
            currentPopulation.Add(Breed(sortedList[sortedList.Count - 1], sortedList[sortedList.Count - 2]));
        }

        for (int i = 0; i < sortedList.Count; i++)
        {
            Destroy(sortedList[i].gameObject);
        }

        generationNumber++;

        currentPopulation = currentPopulation.OrderBy(x => x.FitFactor).ToList();

        if(currentPopulation.Exists(x => x.FitFactor >= 0.99f))
        {
            isStarted = false;

            List<PersonController> cameleons = currentPopulation.FindAll(x => x.FitFactor >= 0.99f);

            cameleons.ForEach(x => x.transform.name = "Cameleon");

            Debug.Log("Cameleon found!");
        }

        PersonController.OnPopulationTrainingStarted();
    }

    private PersonController Breed(PersonController parent1, PersonController parent2)
    {
        Vector3 pos = GetRandomPersonPosition();
        PersonController child = Instantiate(personPrefab, pos, Quaternion.identity).GetComponent<PersonController>();

        float r;
        float g;
        float b;
        Vector3 s;

        float rVar = UnityEngine.Random.Range(-variationDeviation, variationDeviation);
        float gVar = UnityEngine.Random.Range(-variationDeviation, variationDeviation);
        float bVar = UnityEngine.Random.Range(-variationDeviation, variationDeviation);
        float sVar = UnityEngine.Random.Range(-variationDeviation, variationDeviation);

        if (UnityEngine.Random.Range(0, 500) > 5)
        {

            r = UnityEngine.Random.Range(0, 10) < 5 ? parent1.DNA.Color.r : parent2.DNA.Color.r;
            g = UnityEngine.Random.Range(0, 10) < 5 ? parent1.DNA.Color.g : parent2.DNA.Color.g;
            b = UnityEngine.Random.Range(0, 10) < 5 ? parent1.DNA.Color.b : parent2.DNA.Color.b;
            s = (parent1.DNA.Size + parent2.DNA.Size) / 2;

            r += rVar;
            g += gVar;
            b += bVar;
            s = new Vector3(s.x + sVar, s.y + sVar, s.z + sVar);
        }
        else
        {
            Debug.Log("Mutation!");
            child.transform.name = "Mutation";
            DNA dna = GenerateRandomDNA();
            r = dna.Color.r;
            g = dna.Color.g;
            b = dna.Color.b;
            s = dna.Size;
        }

        child.DNA = new DNA(new Color(r,g,b), s);

        return child;
    }

    private Vector3 GetRandomPersonPosition()
    {
        return new Vector3(UnityEngine.Random.Range(-8, 8), UnityEngine.Random.Range(-3.5f, 3.5f), 0);
    }
}
